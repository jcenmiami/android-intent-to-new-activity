package com.ezitapps.intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class OtherIntentActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "message1";
    public static final String EXTRA_MESSAGE2 = "message2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_intent);

        Intent intent = getIntent();
        String receiveMessage1 = intent.getStringExtra(EXTRA_MESSAGE);
        String receiveMessage2 = intent.getStringExtra(EXTRA_MESSAGE2);

        TextView textView1 = (TextView) findViewById(R.id.receivedMessage1TextView);
        TextView textView2 = (TextView) findViewById(R.id.receivedMessage2TextView);

        textView1.setText(receiveMessage1);
        textView2.setText(receiveMessage2);

    }

    public void goBackToMain(View view){
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
