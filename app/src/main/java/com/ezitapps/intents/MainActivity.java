package com.ezitapps.intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editText1;
    EditText editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToNewActivity (View view){

        editText1 = (EditText)findViewById(R.id.editText1);
        editText2 = (EditText)findViewById(R.id.editText2);

        String message1 = editText1.getText().toString();
        String message2 = editText2.getText().toString();

        Intent intent = new Intent(this, OtherIntentActivity.class);

        intent.putExtra(OtherIntentActivity.EXTRA_MESSAGE,message1);
        intent.putExtra(OtherIntentActivity.EXTRA_MESSAGE2,message2);
        startActivity(intent);


    }
}
